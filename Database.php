<?php
class Database{


    private $host='localhost';
    private $username='sweemqmg_kamrul';
    private $password='mmmok1120';
    private $db='sweemqmg_urlshortner';
    private $conn;

    public function __construct(){
        $this->con = mysqli_connect($this->host,$this->username,$this->password,$this->db);
        if ($this->con->connect_error) {
            die("Connection failed: " . $this->con->connect_error);
        }
    }

    public function checkshorturlexist($short_code){
        $result=array();
        $sql="select * from short_urls where short_code='$short_code' LIMIT 1";
        // echo $sql;
        $query=mysqli_query($this->con,$sql);
        while($row=mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $result[]=$row;
        }
        return $result;
    }

    public function  insertShortUrl($shorturl_url,$long_url){
        $sql="INSERT INTO short_urls (long_url, short_code, date_created, counter) VALUES('$long_url','$shorturl_url',NOW(),0)";
        $query=mysqli_query($this->con,$sql);
        return mysqli_insert_id($this->con);
    }


    public function getShortUrlBySlug($short_code){
        $result=array();
        $sql="select * from short_urls where short_code='$short_code' LIMIT 1";
       // echo $sql;
        $query=mysqli_query($this->con,$sql);
        while($row=mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $result[]=$row;
        }
        return $result;
    }

    public function  getShortUrl(){

        $result=array();
        $sql="select * from short_urls";
     //   echo $sql;
        $query=mysqli_query($this->con,$sql);
        while($row=mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $result[]=$row;
        }
        return $result;
    }

    public function getShortUrlBylong_url($url){
        $url=mysqli_real_escape_string($this->con,$url);
        $result=array();
        $sql="select * from short_urls WHERE long_url='$url' LIMIT 1 ";
       // echo $sql;
        $query=mysqli_query($this->con,$sql);
        while($row=mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $result[]=$row;
        }
        return $result;
    }



    public function deleteShortUrl(){
        $sql="DELETE FROM short_urls";
        mysqli_query($this->con,$sql);
    }

    public function close(){
        mysqli_close($this->conn);
    }

}