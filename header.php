<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Welcome to Stock Management System !</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	<link rel="stylesheet" href="css/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />
	<link rel="stylesheet" type="text/css" href="lib/thickbox.css" />
	<link rel="stylesheet" href="date_input.css" type="text/css">
	<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	<link rel="stylesheet" href="css/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	<link rel="stylesheet" href="rok_style.css">

	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="jquery.date_input.js"></script>
	<script type='text/javascript' src='lib/jquery.bgiframe.min.js'></script>
	<script type='text/javascript' src='lib/jquery.ajaxQueue.js'></script>
	<script type='text/javascript' src='lib/thickbox-compressed.js'></script>
	<script type='text/javascript' src='jquery.autocomplete.js'></script>
	<script type='text/javascript' src='localdata.js'></script>
	<script type="text/javascript" src="lib/jquery-ui-1.7.2.custom.min.js"></script>
	<script type="text/javascript" src="jquery-dynamic-form.js"></script>
	<script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
	<script src="js/jquery.validationEngine.js" type="text/javascript"></script>
	<script src="js/jquery.hotkeys-0.7.9.js"></script>
	<script src="js/common.js"> </script>
</head>
<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><table width="960" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
          <tr>
            <td height="90" align="left" valign="top"><img src="images/topbanner.jpg" width="960" height="82"></td>
          </tr>
          <tr>
            <td height="500" align="left" valign="top">
            	<table width="960" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
		              <tr>
		                <td width="130" align="left" valign="top">
						
						<br>

						<strong>Welcome <font color="#3399FF"><?php echo $_SESSION['username']; ?> !</font></strong><br> <br>

						<?php include 'sidemenu.php';?>
						</td>
		                <td height="414" align="center" valign="top">
				
						<?php include_once 'menu.php'; ?>