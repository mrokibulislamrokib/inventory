<?php
session_start(); // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] !='admin'){ // if session variable "username" does not exist.
header("location:index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
else
{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);
	if(isset($_GET['id']) && isset($_GET['table']))
	{
		$id=$_GET['id'];
		$tablename=$_GET['table'];
		$return=$_GET['return'];
		$db->execute("DELETE FROM extra_expenses WHERE id=$id");	
		header("location:$return?msg=Record Deleted Successfully!&id=$id");
	}
}
?>