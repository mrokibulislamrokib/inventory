$(function() {
  $("#datefield").date_input();
   $("#due").date_input();
});

$().ready(function() {

	function log(event, data, formatted) {
		$("<li>").html( !data ? "No match!" : "Selected: " + formatted).appendTo("#result");
	}
	
	function formatItem(row) {
		return row[0] + " (<strong>id: " + row[1] + "</strong>)";
	}
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	
	$("#customer").autocomplete("customer.php", {
		width: 160,
		autoFill: true,
		selectFirst: false
	});

});

$(document).ready(function(){	
	$("#duplicate").dynamicForm("#plus", "#minus", {limit:50, createColor: 'yellow',removeColor: 'red'});
	
});

		function callAutoComplete(idname){
			$("#"+idname).autocomplete("stock.php", {
				width: 160,
				autoFill: true,
				mustMatch: true,
				selectFirst: false
			});
		}
	
	
	function checkDublicateName()
	{	var k=0;
				for (i=0;i<=400;i=i+5)
					{
					if($("#0"+i).length>0)
					{		$k=0;
							 for (j=0;j<=400;j=j+5)
							{
							if($("#0"+j).length>0 && $("#0"+i).val()==$("#0"+j).val())
							{
							 $k++;
							 
							}
							}
						if($k>1)
					{
					alert("Dublicate stock Entry. please remove new and add stock in existing one !");
					
					}
				 	 
					}
					}
					
					
					
					
					
	}

	function callAutoAsignValue(idname)
	{
			
			 var name1 = parseInt(idname,10);
			 
			var quantity1 = name1+1;
			
			 var rate1 =  quantity1+1;
			 var avail1 = rate1+1;
			 var total1 = avail1+1;
			
			 if(parseInt(idname)>0)
			 {
			 quantity1="00"+quantity1;
			 rate1="000"+rate1;
			 avail1="0000"+avail1;
			 total1="00000"+total1;
			 
			 }
			 else
			 {
			  quantity1="00";
			  rate1="000";
			  avail1="0000";
			  total1="00000";
			  
			 }
			 
				 $.post('check_sales_details.php', {stock_name: $("#"+idname).val() },
				function(data){
								
								$("#"+rate1).val(data.rate);
								$("#"+avail1).val(data.availstock);
								$('#supplier_id').val(data.supplier);
								$('#category_id').val(data.category);
								$("#"+quantity1).focus();
							}, 'json');
							
						checkDublicateName();	
							
	}
	
	
	function callQKeyUp(Qidname)
	{		
	
			
			 
			 var quantity = parseInt(Qidname,10);
			 var rate =  quantity+1;
			 var avail = rate+1;
			 var total = avail+1;
			 var rowcount = parseInt((total+1)/5);
			 if(rowcount==0)
			 rowcount=1;
			
			 if(parseInt(Qidname)>0)
			 {
			 quantity="00"+quantity;
			 rate="000"+rate;
			 avail="0000"+avail;
			 total="00000"+total
			 }
			 else
			 {
			  quantity="00";
			  rate="000";
			  avail="0000";
			  total="00000";
			  
			  
			 }
			var result= parseFloat($("#"+quantity).val()) * parseFloat( $("#"+rate).val() );
			result=result.toFixed(2);
			$("#"+total).val(result);
			if(parseFloat($("#"+quantity).val()) > parseFloat($("#"+avail).val()))
			$("#"+quantity).val(parseFloat($("#"+avail).val()));
			
			updateSubtotal();
			
	}
	function balanceCalc()
	{		if(parseFloat($("#payment").val()) > parseFloat($("#subtotal").val()))
			$("#payment").val(parseFloat($("#subtotal").val()));
			
			var result= parseFloat($("#subtotal").val()) - parseFloat( $("#payment").val() );
			result=result.toFixed(2);
			$("#balance").val(result);
			
	}
	function updateSubtotal()
	{					
					var temp=0;
					for (i=4;i<=400;i=i+5)
					{
					if($("#00000"+i).length>0)
					{
					 temp=parseFloat(temp)+parseFloat($("#00000"+i).val());
				 	 
					}
					}
				
			
			var subtotal=parseFloat(temp);
			
			if($("#00000").length>0)
			{
			var firstrowvalue=$("#00000").val();
			
			subtotal=parseFloat(subtotal)+parseFloat(firstrowvalue);
			}
			subtotal=subtotal.toFixed(2);
			$("#subtotal").val(subtotal);
			
			
	}
	
	function callRKeyUp(Ridname)
	{
			var rate = parseInt(Ridname,10);
			 var quantity =  rate-1;
			 var avail = rate+1;
			 var total = avail+1;
			 
			 if(parseInt(Ridname)>0)
			 {
			 quantity="00"+quantity;
			 rate="000"+rate;
			 avail="0000"+avail;
			 total="00000"+total
			 
			 }
			 else
			 {
			  quantity="00";
			  rate="000";
			  avail="0000";
			  total="00000";
			  
			 }
			
			var result= parseFloat($("#"+quantity).val()) * parseFloat( $("#"+rate).val() );
			result=result.toFixed(2);
			$("#"+total).val(result);
			if(parseFloat($("#"+quantity).val()) > parseFloat($("#"+avail).val()))
			$("#"+quantity).val(parseFloat($("#"+avail).val()));
			
			updateSubtotal();
	
	}
		
		
		$(document).ready(function() {
			// SUCCESS AJAX CALL, replace "success: false," by:     success : function() { callSuccessFunction() }, 
			 $("#billnumber").focus();
			
			 $("#customer").blur(function(){
			
				 $.post('check_customer_details.php', {stock_name1: $(this).val() },
				function(data){
					$("#customer_id").val(data.CUSID);
					$("#address").val(data.address);
					$("#contact1").val(data.contact1);
					$("#contact2").val(data.contact2);
					if(data.address!=undefined)
					$("#0").focus();
					
				}, 'json');
			});
		});