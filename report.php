<?php
session_start(); // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] !='admin'){ // if session variable "username" does not exist.
  header("location:index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}else{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);
  include_once "header.php";
?>
				
				<br>
<br> 
<strong>Reports</strong><br>
<br>
<table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <form action="sales_report.php" method="post" name="sales_report" id="sales_report" target="myNewWinsr">
      <td><strong>Sales Report </strong></td>
      <td>From</td>
      <td><input name="from_sales_date" type="text" id="from_sales_date" style="width:80px;"></td>
      <td>To</td>
      <td><input name="to_sales_date" type="text" id="to_sales_date" style="width:80px;"></td>
    
    <!--   <td>
        <select name="exp_name" id="stock_name">
          <option value="">All</option>
          <?php 
          //  $result = $db->query("SELECT Distinct name FROM stock_avail");
           // while ($line = $db->fetchNextObject($result)) {
          ?>
            <option value="<?php // echo $line->name;?>"> <?php // echo $line->name;?> </option>
    
        <?php // } ?>
            
          </select>
    </td> -->

      <td>
        <select name="stock_product_name" id="stock_product_name">
          <option value="">All</option>
          <?php 
            //$result = $db->query("SELECT Distinct stock_supplier_name FROM stock_entries");
              $result = $db->query("SELECT Distinct * FROM supplier_details");
              while ($line = $db->fetchNextObject($result)) {
          ?>
           <!--  <option value="<?php //echo $line->stock_supplier_name;?>"> <?php// echo $line->stock_supplier_name;?> </option> -->
          
           <option value="<?php echo $line->id;?>"> <?php echo $line->supplier_name;?> </option> -->

          <?php } ?>
          
        </select>
        
      </td>

      <td>&nbsp;</td>

      <td>
        <select name="stock_supplier_product_name" id="stock_supplier_product_name">
          
           <option value="">All</option>
          
        </select>
        
      </td>
      <td><input name="submit" type="button" value="Show" onClick='sales_report_fn();'></td>
    </form>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <form action="purchase_report.php" method="post" name="purchase_report" target="_blank">
      <td><strong>Purchase Report </strong></td>
      <td>From</td>
      <td><input name="from_purchase_date" type="text" id="from_purchase_date" style="width:80px;"></td>
      <td>To</td>
      <td><input name="to_purchase_date" type="text" id="to_purchase_date" style="width:80px;"></td>
     <!--  <td>&nbsp;</td> -->
      <td>
        <select name="purchase_product_name" id="purchase_product_name">
          <option value="">All</option>
          <?php 
            //$result = $db->query("SELECT Distinct stock_supplier_name FROM stock_entries");
              $result = $db->query("SELECT Distinct * FROM supplier_details");
              while ($line = $db->fetchNextObject($result)) {
          ?>
           <!--  <option value="<?php //echo $line->stock_supplier_name;?>"> <?php// echo $line->stock_supplier_name;?> </option> -->
          
           <option value="<?php echo $line->id;?>"> <?php echo $line->supplier_name;?> </option> -->

          <?php } ?>
          
        </select>
        
      </td>

      <td>&nbsp;</td>

      <td>
        <select name="purchase_supplier_product_name" id="purchase_supplier_product_name">
          
           <option value="">All</option>
          
        </select>
        
      </td>




      <td><input name="submit" type="button" value="Show" onClick='purchase_report_fn();'></td>
    </form>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <form action="sales_purchase_report.php" method="post" name="sales_purchase_report" target="_blank">
      <td><strong>Purchase Stocks </strong></td>
      <td>From</td>
      <td><input name="from_sales_purchase_date" type="text" id="from_sales_purchase_date" style="width:80px;"></td>
      <td>To</td>
      <td><input name="to_sales_purchase_date" type="text" id="to_sales_purchase_date" style="width:80px;"></td>
      <td><input name="submit" type="button" value="Show" onClick='sales_purchase_report_fn();'></td>
    </form>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

   <tr>
    <form action="sales_purchase_report.php" method="post" name="sales_purchase_report" target="_blank">
      <td><strong>Company Wise  </strong></td>
      <td>From</td>
      <td><input name="from_sales_company_purchase_date" type="text" id="from_sales_company_purchase_date" style="width:80px;"></td>
      <td>To</td>
      <td><input name="to_sales_company_purchase_date" type="text" id="to_sales_company_purchase_date" style="width:80px;"></td>




      <td>
      <!--   <select name="stock_product_name" id="stock_product_name"> -->
        <select name="company_id" id="company_id">
          <option value="">All</option>
          <?php 
            //$result = $db->query("SELECT Distinct stock_supplier_name FROM stock_entries");
              $result = $db->query("SELECT Distinct * FROM supplier_details");
              while ($line = $db->fetchNextObject($result)) {
          ?>
           <!--  <option value="<?php //echo $line->stock_supplier_name;?>"> <?php// echo $line->stock_supplier_name;?> </option> -->
          
           <option value="<?php echo $line->id;?>"> <?php echo $line->supplier_name;?> </option> -->

          <?php } ?>
          
        </select>
        
      </td>

      <td><input name="submit" type="button" value="Show" onClick='sales_company_purchase_report_fn();'></td>
    </form>
  </tr>

    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


<tr>
    <form action="payment_report.php" method="post" name="payment_report" target="_blank">
      <td><strong>Payment Report  </strong></td>
      <td>From</td>
      <td><input name="from_payment_date" type="text" id="from_payment_date" style="width:80px;"></td>
      <td>To</td>
      <td><input name="to_payment_date" type="text" id="to_payment_date" style="width:80px;"></td>


      <td>
        <select name="payment_name" id="payment_name">
          <option value="">All</option>
          <?php 
        $result = $db->query("SELECT Distinct name FROM payment");
        while ($line = $db->fetchNextObject($result)) {
        ?>
          
          <option value="<?php echo $line->name;?>"> <?php echo $line->name;?> </option>

      <?php } ?>
          
        </select>
      </td>
      <td><input name="submit" type="button" value="Show" onClick='payment_report_fn();'></td>
    </form>
  </tr>


  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

<tr>
    <form action="expense_report.php" method="post" name="expense_report" target="_blank">
      <td><strong>Expense Report  </strong></td>
      <td>From</td>
      <td><input name="from_expense_date" type="text" id="from_expense_date" style="width:80px;"></td>
      <td>To</td>
      <td><input name="to_expense_date" type="text" id="to_expense_date" style="width:80px;"></td>
      </td>
            <td>
        <select name="exp_name" id="exp_name">
           <option value="">All</option>
          <?php 
        $result = $db->query("SELECT Distinct name FROM extra_expenses");
        while ($line = $db->fetchNextObject($result)) {
        ?>
        
          <option value="<?php echo $line->name;?>"> <?php echo $line->name;?> </option>

      <?php } ?>
          
        </select>
      </td>
      <td><input name="submit" type="button" value="Show" onClick='expense_report_fn();'></td>
    </form>
  </tr>



  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="183">Total Number of Stocks  </td>
    <td width="84"><strong><?php echo  $count = $db->countOfAll("stock_avail");?>&nbsp;</strong></td>
    <td width="110">Payment Pending: </td>
    <td width="110"><strong><?php
	
echo $db->queryUniqueValue("select sum(balance) FROM  stock_entries where count1=1 and type='entry'");

	
?></strong></td>
    <td width="113">&nbsp;</td>
    <td width="113">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Tatal Sales:</td>
    <td><strong><?php echo  $age = $db->queryUniqueValue("SELECT sum(subtotal) FROM stock_sales where count1=1 ");?></strong></td>
    <td>Outstanding Amount: </td>
    <td><strong><?php echo $db->queryUniqueValue("select sum(balance) FROM  stock_sales where count1=1");
	?></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Total number of Suppliers </td>
    <td><strong><?php echo $count = $db->countOfAll("supplier_details");?></strong></td>
    <td>Total Number <br>
      of Customers </td>
    <td><strong><?php echo $count = $db->countOfAll("customer_details");?></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br><br>
<br>
                </td>
              </tr>
            </table>
			
		</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#72C9F4"><span class="style1">Developed by <a href="http://www.fb.com/shuyeb.ahmed">Shuyeb </a> & <a href="http://www.fb.com/rokibulislam.rokib.71">Rokib</a></span></td>
          </tr>
        </table></td>
        <td>
      <?php include 'sidemenu-right.php';?>
    </td>
      </tr>
    </table></td>
  </tr>
</table>

  
  <script type="text/javascript">
    $(function() {
      $("#from_sales_date").date_input();
      $("#to_sales_date").date_input();
      $("#from_purchase_date").date_input();
      $("#to_purchase_date").date_input();
      $("#from_sales_purchase_date").date_input();
      $("#to_sales_purchase_date").date_input();
      $("#from_stock_sales_date").date_input();
      $("#to_stock_sales_date").date_input();
      $('#from_sales_company_purchase_date').date_input();
      $('#to_sales_company_purchase_date').date_input();
      $('#from_payment_date').date_input();
      $('#to_payment_date').date_input();
      $('#from_expense_date').date_input();
      $('#to_expense_date').date_input();
    });

function sales_report_fn() { 
 window.open("sales_report.php?from_sales_date="+$('#from_sales_date').val()+"&to_sales_date="+$('#to_sales_date').val() + "&stock_product_name="+ $('#stock_product_name').val() +"&stock_supplier_product_name="+ $('#stock_supplier_product_name').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes"); 
}
function purchase_report_fn() { 
 window.open("purchase_report.php?from_purchase_date="+$('#from_purchase_date').val()+"&to_purchase_date="+$('#to_purchase_date').val() +"&purchase_payment_name="+$('#purchase_payment_name').val() + "&purchase_product_name="+ $('#purchase_product_name').val() +"&purchase_supplier_product_name="+ $('#purchase_supplier_product_name').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes"); 
} 

function sales_purchase_report_fn() 
{ 
 window.open("all_report.php?from_sales_purchase_date="+$('#from_sales_purchase_date').val()+"&to_sales_purchase_date="+$('#to_sales_purchase_date').val()+"&company_id="+$('#company_id').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes"); 
   
} 

function stock_sales_report_fn() 
{ 
 window.open("sales_stock_report.php?from_stock_sales_date="+$('#from_stock_sales_date').val()+"&to_stock_sales_date="+$('#to_stock_sales_date').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes"); 
   
} 

function sales_company_purchase_report_fn(){
  window.open("sales_company_purchase_report.php?from_sales_company_purchase_date="+$('#from_sales_company_purchase_date').val()+"&to_sales_company_purchase_date="+$('#to_sales_company_purchase_date').val()+"&company_id="+$('#company_id').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");    
}

function payment_report_fn(){

    window.open("payment_report.php?from_payment_date="+$('#from_payment_date').val()+"&to_payment_date="+$('#to_payment_date').val()+"&payment_name="+$('#payment_name').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes");    
}


function expense_report_fn(){ 
 window.open("expense_report.php?from_expense_date="+$('#from_expense_date').val()+"&to_expense_date="+$('#to_expense_date').val()+"&exp_name="+$('#exp_name').val(),"myNewWinsr","width=820,height=800,toolbar=0,menubar=no,status=no,resizable=yes,location=no,directories=no,scrollbars=yes"); 
   
}

</script>



  <script>
      
      $(document).ready(function(){
        
        $('#purchase_product_name').change(function(){
          
            var country_id = $('#purchase_product_name').val();

            console.log(country_id);
          
            if(country_id != 0){
              $.ajax({
                type:'post',
                url:'getvalue.php',
                data:{id:country_id},
                cache:false,
                success: function(returndata){
                  console.log(returndata);
                  $('#purchase_supplier_product_name').append(returndata);
                }
              });
            }

        });

      });


    $(document).ready(function(){
        
        $('#stock_product_name').change(function(){
          
            var country_id = $('#stock_product_name').val();

            console.log(country_id);
          
            if(country_id != 0){
              $.ajax({
                type:'post',
                url:'getvalue.php',
                data:{id:country_id},
                cache:false,
                success: function(returndata){
                  console.log(returndata);
                  $('#stock_supplier_product_name').append(returndata);
                }
              });
            }

        });

      });

  </script>
</body>
</html>
<?php
}
?>