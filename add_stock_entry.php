<?php
session_start(); // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] !='admin'){ // if session variable "username" does not exist.
header("location:index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}else{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Welcome to Stock Management System !</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<link rel="stylesheet" href="css/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<script src="js/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="jquery.date_input.js"></script>
<link rel="stylesheet" href="date_input.css" type="text/css">
<script type="text/javascript">$(function() {
  $("#datefield").date_input();
});</script>

		<script type='text/javascript' src='lib/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='lib/thickbox-compressed.js'></script>
<script type='text/javascript' src='jquery.autocomplete.js'></script>
<script type='text/javascript' src='localdata.js'></script>

<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="lib/thickbox.css" />
	
<script type="text/javascript">
$().ready(function() {

	function log(event, data, formatted) {
		$("<li>").html( !data ? "No match!" : "Selected: " + formatted).appendTo("#result");
	}
	
	function formatItem(row) {
		return row[0] + " (<strong>id: " + row[1] + "</strong>)";
	}
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	


	$("#singleBirdRemote").autocomplete("stock.php", {
		width: 160,
		autoFill: true,
		selectFirst: false
	});

	/*$("#supplier").autocomplete("supplier1.php", {
		width: 160,
		autoFill: true,
		selectFirst: false,
		select: function( event, ui ) {

		}
	});
	*/

	/*source: function( request, response ) { 
		$.ajax({
			url: "",
			dataType: "json",
			type: "GET",
			data: {term: $('#emp').val().toUpperCase()},
			success: function( xmlResponse ) {
			
			}
		});
	},*/ // end of source

	$("#supplier").autocomplete("supplier1.php", {
		width: 160,
		autoFill: true,
		selectFirst: false,
		select: function( event, ui ) {
			$("input#empSelected").val(ui.item.id);
		}
	});


	$("#category").autocomplete("category.php", {
		width: 160,
		autoFill: true,
		selectFirst: false
		
	});

	var partTags = [{"label":"Part1","dwg":"A"},{"label":"Part2","dwg":"B"}]


	$( "#supplier" ).autocomplete({
		/*width: 160,
		autoFill: true,
		selectFirst: false,*/
		//var dict = [];
		
	var res=$.ajax({
          url: "supplier1.php",
          method: 'GET',
          dataType: "json",
          data: {
            q: request.term
          },
          success: function( data ) {
          	//response(data);
          	/*$.each(data, function() {
		      dict.push(this.toString());
		    });
		    response(dict)*/

		    return data;
          }
        });

	    /*source: function( request, response ) {
	        $.ajax({
	          url: "supplier1.php",
	          method: 'GET',
	          dataType: "json",
	          data: {
	            q: request.term
	          },
	          success: function( data ) {
	          	response($.map(data, function (pn) {
		            return {
		                value: pn.value,
		                label: pn.label
		            };
		        }));
	          	//response(data);
	          	$.each(data, function() {
			      dict.push(this.toString());
			    });
			    response(dict)
	          }
	        });
      	},*/
      	source: res,
        minLength: 2
    });

	/*$( "#supplier" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "supplier1.php",
          dataType: "jsonp",
          data: {
            q: request.term
          },
          success: function( data ) {
          	console.log($.parseJSON(data));
			 response($.parseJSON(data));
			// response(data);
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
      }
    });*/
	


	$("#clear").click(function() {
		$(":input").unautocomplete();
	});
});


</script>

		<script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="js/jquery.validationEngine.js" type="text/javascript"></script>
		 <script src="js/jquery.hotkeys-0.7.9.js"></script>
		 <script src="js/common.js"> </script>
		<link rel="stylesheet" href="rok_style.css">
		
		<script>	
		
		
		
		
		
		$(document).ready(function() {
			// SUCCESS AJAX CALL, replace "success: false," by:     success : function() { callSuccessFunction() }, 
			 $("#singleBirdRemote").focus();
			 $("#singleBirdRemote").blur(function()
			{
			
				 $.post('check_stock_details.php', {stock_name: $(this).val() },
				function(data){
				
								// if(data=='no') //if username not avaiable
		 						// {
								//  $("#category").focus();
								// }
								$("#category").val(data.category);
								$("#supplier").val(data.supplier);
								$("#buyingrate").val(data.buyingrate);
								$("#sellingrate").val(data.sellingprice);
								
								$("#available").val(data.available);
								$("#quantity").focus();
							}, 'json');
							
			});
</script>
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><table width="960" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
          <tr>
            <td height="90" align="left" valign="top"><img src="images/topbanner.jpg" width="960" height="82"></td>
          </tr>
          <tr>
            <td height="800" align="left" valign="top"><table width="960" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
              <tr>
                <td width="130" align="left" valign="top">
				
				<br>

				<strong>Welcome <font color="#3399FF"><?php echo $_SESSION['username']; ?> !</font></strong><br> <br>
				<?php include 'sidemenu.php';?>


	
				
				
				</td> <td height="500" align="center" valign="top">


<?php include_once 'menu.php'; ?>
<?php
				if(isset($_POST['name']))

            {
			
			$name=mysql_real_escape_string($_POST['name']);
			$category=mysql_real_escape_string($_POST['category']);
			$buyingrate=mysql_real_escape_string($_POST['buyingrate']);
			$sellingrate=mysql_real_escape_string($_POST['sellingrate']);
			$suplier=mysql_real_escape_string($_POST['suplier']);
			//$uom=mysql_real_escape_string($_POST['uom']);
			$quantity=mysql_real_escape_string($_POST['quantity']);
			$count = $db->countOf("stock_avail", "name='$name'");
			$selected_date=$_POST['date'];
		  	$selected_date=strtotime( $selected_date );
			$mysqldate = date( 'Y-m-d H:i:s', $selected_date );
			$username = $_SESSION['username'];

				$max = $db->maxOfAll("id", "stock_details");
					  $max=$max+1;
					  $autoid="SD".$max."";
			   $max1 = $db->maxOfAll("id", "stock_details");
					  $max1=$max1+1;
					  $autoid1="SD".$max1."";
			if($count==0)
			{
			$db->query("insert into stock_avail(name,quantity) values('$name',$quantity)");
			echo "<br><font color=green size=+1 >New Stock Entry Inserted !</font>" ;
			   
			$db->query("insert into stock_details(stock_id,stock_name,stock_quatity,supplier_id,company_price,selling_price,category) values(' $autoid','$name',0,'$suplier',$buyingrate,$sellingrate,'$category')");
			$db->query("insert into stock_entries (stock_id,stock_name,stock_supplier_name,category,quantity,company_price,selling_price,opening_stock,closing_stock,date,username,type) values('$autoid1','$name','$suplier','$category',$quantity,$buyingrate,$sellingrate,0,$quantity,'$mysqldate','$username','entry')");
			}
			else if($count==1) 
			{
				$amount = $db->queryUniqueValue("SELECT quantity FROM stock_avail WHERE name='$name'");
				$amount1 = $amount + $quantity;
				$db->execute("UPDATE stock_avail SET quantity=$amount1 WHERE name='$name'");
				$db->query("insert into stock_entries (stock_id,stock_name,stock_supplier_name,category,quantity,company_price,selling_price,opening_stock,closing_stock,date,username,type) values('$autoid1','$name','$suplier','$category',$quantity,$buyingrate,$sellingrate,$amount,$amount1,'$mysqldate','$username','entry')");
				
				echo "<br><font color=green size=+1 >$quantity Quantity Added in $name !</font>" ;
			}
			
			
			}
				
				?>
				
				<br>
<br>

				
				<form name="form1" method="post" id="form1" action="">
                  
                  <p align="center"><strong>Add New Stock Entry </strong> - Add New ( Control +A)</p>
                  <table width="600"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">ID <?php
					  $max = $db->maxOfAll("id","stock_entries");
					  $max=$max+1;
					  $autoid="SE".$max."";
					  ?></td>
					  <td width="150"><input name="id" type="text" id="id" readonly="" value="<?php echo $autoid; ?>"></td>
					  <td width="150">Date</td>
					 
                      <td width="150"> <input type="text" id="datefield" name="date" class="date_input" value="<?php echo date('d-m-Y');?>"></td>
                    </tr>
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150"><strong>Name</strong></td>
                      <td width="150"><input name="name" type="text" id="singleBirdRemote" class="validate[required,length[0,100]] text-input"></td>
                      <td width="150">Category</td>
                      <td width="150"><input name="category" id="category" type="text" value="" ></td>
                    </tr>
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">Supplier</td>
                      <td width="150"><input name="suplier" type="text" id="supplier" class="validate[optional,length[0,100]] text-input"></td>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Buying Rate</td>
                      <td><input name="buyingrate" type="text" id="buyingrate"  class="validate[required,custom[onlyNumber],lengthCheck[6]] text-input" ></td>
                      <td>Selling Rate </td>
                      <td><input name="sellingrate" type="text" id="sellingrate"  class="validate[required,custom[onlyNumber],lengthCheck[6]] text-input" ></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><strong>Quantity</strong></td>
                      <td><input name="quantity" type="text" id="quantity"  class="validate[required,custom[onlyNumber],lengthCheck[6]] text-input" ></td>
                      <td>Available Quantity </td>
                      <td><input name="available" type="text" id="available" value=""  readonly=""></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="right">&nbsp;</td>
                      <td align="right"><input type="reset" name="Reset" value="Reset">
&nbsp;&nbsp;&nbsp;</td>
                      <td> &nbsp;&nbsp;&nbsp;
                          <input type="submit" name="Submit" value="Save"></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center">&nbsp;</td>
                      <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Control + R) </td>
                      <td align="left"> &nbsp;&nbsp;( Control + S ) </td>
                      <td align="left">&nbsp;</td>
                    </tr>
                  </table>
                </form></td>
              </tr>
            </table>
			
		</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#72C9F4"><span class="style1"><a href="http://www.pluskb.com">Developed by PlusKB Innovations</a></span></td>
          </tr>
        </table></td>
        <td>
			<?php include 'sidemenu-right.php';?>
		</td>
      </tr>
    </table></td>
  </tr>
</table>

</body>
</html>
<?php
}
?>