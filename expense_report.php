<?php
session_start(); // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] !='admin'){ // if session variable "username" does not exist.
header("location:index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}
else
{
if(isset($_GET['from_expense_date']) && isset($_GET['to_expense_date'])  &&  $_GET['from_expense_date']!='' && $_GET['to_expense_date']!='')
{


	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);
			$selected_date=$_GET['from_expense_date'];
		  	$selected_date=strtotime( $selected_date );
			$mysqldate = date( 'Y-m-d H:i:s', $selected_date );
$fromdate=$mysqldate;
			$selected_date=$_GET['to_expense_date'];
		  	$selected_date=strtotime( $selected_date );
			$mysqldate = date( 'Y-m-d H:i:s', $selected_date );

$todate=$mysqldate;


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Sale Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css" media="print">
.hide{display:none}
</style>
<script type="text/javascript">
function printpage() {
document.getElementById('printButton').style.visibility="hidden";
window.print();
document.getElementById('printButton').style.visibility="visible";  
}
</script>
<body>
<input name="print" type="button" value="Print" id="printButton" onClick="printpage()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
      <table width="695" border="0" cellspacing="0" cellpadding="0">
       
        <tr>
          <td height="30" align="center"><strong>PAYMENT Report for <?php echo $name;?> </strong></td>
        </tr>
        <tr>
          <td height="30" align="center">&nbsp;</td>
        </tr>
        <tr>
          <td align="right"><table width="300" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="150"><strong>Total Payment </strong></td>
          
              <?php 
              if(isset($_GET['exp_name']) && $_GET['exp_name']!=''){
                $name=$_GET['exp_name'];
                $sql="SELECT sum(total) FROM extra_expenses where name='$name' && date BETWEEN '$fromdate' AND '$todate' ";
              }else{
                $sql="SELECT sum(total) FROM extra_expenses where date BETWEEN '$fromdate' AND '$todate' ";
              } ?>
              
              <td width="150">&nbsp;<?php echo  $total_pament = $db->queryUniqueValue($sql);?></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td width="45"><hr></td>
        </tr>
        <tr>
          <td height="20"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="45"><strong>From</strong></td>
                <td width="393">&nbsp;<?php echo $_GET['from_expense_date']; ?></td>
                <td width="41"><strong>To</strong></td>
                <td width="116">&nbsp;<?php echo $_GET['to_expense_date']; ?></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td width="45"><hr></td>
        </tr>
        <tr>
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><strong>ID</strong></td>
                <td><strong>Name</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Amount</strong></td>
              </tr>
			  <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>



        <?php 
              if(isset($_GET['exp_name']) && $_GET['exp_name']!=''){
                $name=$_GET['exp_name'];
                $result = $db->query("SELECT * FROM extra_expenses where name='$name' && date BETWEEN '$fromdate' AND '$todate' ");
              }else{
                $result = $db->query("SELECT * FROM extra_expenses where date BETWEEN '$fromdate' AND '$todate' ");
              } 
              while ($line = $db->fetchNextObject($result)) {
        ?>

		
				<tr>
                
                <td><?php echo $line->id; ?></td>
                <td><?php echo $line->name; ?></td>
               <td><?php  $mysqldate=$line->date;
    $phpdate = strtotime( $mysqldate );
    $phpdate = date("d/m/Y",$phpdate);
    echo $phpdate; ?></td>
                <td><?php echo $line->total; ?></td>
              </tr>
			  	

<?php
}
			  ?>
          </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>

</body>
</html>
<?php
}
else
echo "Please from and to date to process report";
}
?>