<?php
session_start(); // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] !='admin'){ // if session variable "username" does not exist.
	header("location:index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}else{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);
	include_once "header.php";
?>
<br>
<br>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><form action="" method="post" name="search" >
<input name="searchtxt" type="text"> 
&nbsp;&nbsp;<input name="Search" type="submit" value="Search">
</form></td>
    <td><form action="" method="get" name="page">
Page per Record<input name="limit" type="text"  style="margin-left:5px;" value="<?php if(isset($_GET['limit'])) echo $_GET['limit']; else echo "10"; ?>" size="3" maxlength="3">
<input name="go" type="submit" value="Go">
<input type="button" name="selectall" value="SelectAll" onClick="checkAll()"  style="margin-left:5px;"/>
<input type="button" name="unselectall" value="DeSelectAll" onClick="uncheckAll()" style="margin-left:5px;" />
<input name="dsubmit" type="button" value="Delete Selected" style="margin-left:5px;" onclick="return confirmDeleteSubmit()"/></form></td>
  </tr>
</table>


				<?php 



$SQL = "SELECT * FROM  stock_details";
if(isset($_POST['Search']) AND trim($_POST['searchtxt'])!="")
{

$SQL = "SELECT COUNT(*) as num FROM stock_details WHERE stock_name LIKE '%".$_POST['searchtxt']."%' OR supplier_id LIKE '%".$_POST['searchtxt']."%' OR category LIKE '%".$_POST['searchtxt']."%' OR date LIKE '%".$_POST['searchtxt']."%'";


}

	$tbl_name="stock_details";		//your table name

	// How many adjacent pages should be shown on each side?

	$adjacents = 3;

	

	/* 

	   First get total number of rows in data table. 

	   If you have a WHERE clause in your query, make sure you mirror it here.

	*/
	
	
	

	$query = "SELECT COUNT(*) as num FROM $tbl_name";
	if(isset($_POST['Search']) AND trim($_POST['searchtxt'])!="")
{

$query = "SELECT COUNT(*) as num FROM stock_details WHERE stock_name LIKE '%".trim($_POST['searchtxt'])."%' OR supplier_id LIKE '%".trim($_POST['searchtxt'])."%' OR category LIKE '%".trim($_POST['searchtxt'])."%' OR date LIKE '%".trim($_POST['searchtxt'])."%'";



}


	$total_pages = mysql_fetch_array(mysql_query($query));

	$total_pages = $total_pages[num];

	

	/* Setup vars for query. */

	$targetpage = "view_stock_details.php"; 	//your file name  (the name of this file)
	
	
	$limit = 10; 								//how many items to show per page
	
	if(isset($_GET['limit']))
	$limit=$_GET['limit'];
	
	$page = $_GET['page'];

	if($page) 

		$start = ($page - 1) * $limit; 			//first item to display on this page

	else

		$start = 0;								//if no page var is given, set start to 0

	

	/* Get data. */

	$sql = "SELECT * FROM stock_details ORDER BY date desc LIMIT $start, $limit ";
	if(isset($_POST['Search']) AND trim($_POST['searchtxt'])!="")
{

$sql = "SELECT * FROM stock_details WHERE stock_name LIKE '%".trim($_POST['searchtxt'])."%' OR supplier_id LIKE '%".trim($_POST['searchtxt'])."%' OR category LIKE '%".trim($_POST['searchtxt'])."%' OR date LIKE '%".trim($_POST['searchtxt'])."%' ORDER BY date desc LIMIT $start, $limit ";


}



	$result = mysql_query($sql);

	

	/* Setup page vars for display. */

	if ($page == 0) $page = 1;					//if no page var is given, default to 1.

	$prev = $page - 1;							//previous page is page - 1

	$next = $page + 1;							//next page is page + 1

	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.

	$lpm1 = $lastpage - 1;						//last page minus 1

	

	/* 

		Now we apply our rules and draw the pagination object. 

		We're actually saving the code to a variable in case we want to draw it more than once.

	*/

	$pagination = "";

	if($lastpage > 1)

	{	

		$pagination .= "<div class=\"pagination\">";

		//previous button

		if ($page > 1) 

			$pagination.= "<a href=\"$targetpage?page=$prev&limit=$limit\">� previous</a>";

		else

			$pagination.= "<span class=\"disabled\">� previous</span>";	

		

		//pages	

		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up

		{	

			for ($counter = 1; $counter <= $lastpage; $counter++)

			{

				if ($counter == $page)

					$pagination.= "<span class=\"current\">$counter</span>";

				else

					$pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit\">$counter</a>";					

			}

		}

		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some

		{

			//close to beginning; only hide later pages

			if($page < 1 + ($adjacents * 2))		

			{

				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)

				{

					if ($counter == $page)

						$pagination.= "<span class=\"current\">$counter</span>";

					else

						$pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit\">$counter</a>";					

				}

				$pagination.= "...";

				$pagination.= "<a href=\"$targetpage?page=$lpm1&limit=$limit\">$lpm1</a>";

				$pagination.= "<a href=\"$targetpage?page=$lastpage&limit=$limit\">$lastpage</a>";		

			}

			//in middle; hide some front and some back

			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))

			{

				$pagination.= "<a href=\"$targetpage?page=1&limit=$limit\">1</a>";

				$pagination.= "<a href=\"$targetpage?page=2&limit=$limit\">2</a>";

				$pagination.= "...";

				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)

				{

					if ($counter == $page)

						$pagination.= "<span class=\"current\">$counter</span>";

					else

						$pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit\">$counter</a>";					

				}

				$pagination.= "...";

				$pagination.= "<a href=\"$targetpage?page=$lpm1&limit=$limit\">$lpm1</a>";

				$pagination.= "<a href=\"$targetpage?page=$lastpage&limit=$limit\">$lastpage</a>";		

			}

			//close to end; only hide early pages

			else

			{

				$pagination.= "<a href=\"$targetpage?page=1&limit=$limit\">1</a>";

				$pagination.= "<a href=\"$targetpage?page=2&limit=$limit\">2</a>";

				$pagination.= "...";

				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)

				{

					if ($counter == $page)

						$pagination.= "<span class=\"current\">$counter</span>";

					else

						$pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit\">$counter</a>";					

				}

			}

		}

		

		//next button

		if ($page < $counter - 1) 

			$pagination.= "<a href=\"$targetpage?page=$next&limit=$limit\">next �</a>";

		else

			$pagination.= "<span class=\"disabled\">next �</span>";

		$pagination.= "</div>\n";		

	}

?>
					<?php if(isset($_GET['msg'])) echo "Record ID:[ ".$_GET['id']." ] <center>".$_GET['msg']."</center>"; 
					
					if(isset($_GET['cmsg'])) echo "<center>".$_GET['cmsg']."</center>";
					?>


      
	 <form name="deletefiles" action="deleteselected.php" method="post">
	 <input name="table" type="hidden" value="stock_details">
	 <input name="return" type="hidden" value="view_stock_details.php">
	  
	  
	  <table width="700" border="0" cellspacing="0" cellpadding="0">

      <tr>

        <td bgcolor="#0099FF"><div align="center"><strong><span class="style1">View Stock Details </span></strong></div></td>

      </tr>

      <tr>

        <td>&nbsp;</td>

      </tr>

      <tr>

        <td align="center"><table width="100%"  border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="100"><strong>Stock Name </strong></td>

            <td width="100"><strong>Date</strong></td>

            <td width="100"><strong>Supplier</strong></td>

            <td width="100"><strong>Selling <br>
              Price <br>
</strong></td>

            <td width="100"><strong>Avail<br>
Stock<br>
</strong></td>

            <td width="100"><strong>View/Edit</strong></td>
            <td width="100"><strong>Delete</strong></td>
            <td width="100"><strong>Select</strong></td>
          </tr>

		  

		  

		  <?php

	 

								while($row = mysql_fetch_array($result))

		{

		

		 $mysqldate=$row['date'];

 		$phpdate = strtotime( $mysqldate );

 		$phpdate = date("d/m/Y",$phpdate);



										 ?>

  											<tr>



       	<td width="100"><?php echo $row['stock_name']; ?></td>

        <td width="100"><?php echo $phpdate; ?></td>

       <!--  <td width="100"><?php // echo  $row['supplier_id']; ?></td> -->

       	 <td width="100"><?php $supplier = $db->queryUniqueValue("SELECT supplier FROM stock_avail WHERE name='".$row['stock_name']."'"); 

       	 $supplier = $db->queryUniqueValue("SELECT supplier_name FROM supplier_details WHERE id='$supplier'"); 

       	 echo $supplier; ?></td>

        

        <td width="100"><?php echo $row['selling_price']; ?></td>

        <td width="100"><?php $quantity = $db->queryUniqueValue("SELECT quantity FROM stock_avail WHERE name='".$row['stock_name']."'"); echo $quantity; ?></td>

	<td width="100"> <a href="update_stock_details.php?id=<?php echo $row['id'];?>"><img src="images/edit-icon.png" border="0" alt="delete"></a></td>
							<td width="100"><a onclick="return confirmSubmit()"
 href="delete.php?id=<?php echo $row['id']; ?>&table=stock_details&return=view_stock_details.php"><img src="images/delete.png" border="0" alt="delete"></a></td>
<td width="100">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" value="<?php echo $row['id']; ?>" name="checklist[]" /></td>
								  </tr> 

											 

											 

										

                                             

                                             <?php

												

											 

									  }

							  			

		              

						  

	

 



?>

		  

		  

		  

		  

		  

        </table></td>

      </tr>

      <tr>

        <td>&nbsp;</td>

      </tr>

      <tr>

        <td align="center">&nbsp;</td>

      </tr>

      <tr>

        <td align="center"><div style="margin-left:20px;"><?php echo $pagination; ?></div></td>

      </tr>

      <tr>

        <td align="center">&nbsp;</td>

      </tr>

      <tr>

        <td>&nbsp;</td>

      </tr>

      <tr>

        <td align="center">&nbsp; </td>

      </tr>

      <tr>

        <td>&nbsp;</td>

      </tr>

    </table>
	
	</form>

	



	

	</td>

  </tr>

</table>



</td>
              </tr>
            </table>
			
		</td>
          </tr>
         <tr>
            <td height="30" align="center" bgcolor="#72C9F4"><span class="style1">Developed by <a href="http://www.fb.com/shuyeb.ahmed">Shuyeb </a> & <a href="http://www.fb.com/rokibulislam.rokib.71">Rokib</a></span></td>
          </tr>
        </table></td>
        <td>
			<?php include 'sidemenu-right.php';?>
		</td>
      </tr>
    </table></td>
  </tr>
</table>
<script LANGUAGE="JavaScript">
<!--
// Nannette Thacker http://www.shiningstar.net
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to Delete this Entry?");
if (agree)
	return true ;
else
	return false ;
}

function confirmDeleteSubmit()
{
var agree=confirm("Are you sure you wish to Delete Seletec Record?");
if (agree)
	
document.deletefiles.submit();
else
	return false ;
}


function checkAll()
{

	var field=document.forms.deletefiles;
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll()
{
	var field=document.forms.deletefiles;
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}
// -->
</script>
</body>
</html>
<?php
}
?>