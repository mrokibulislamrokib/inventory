<?php
session_start(); // Use session variable on this page. This function must put on the top of page.
if(!isset($_SESSION['username']) || $_SESSION['usertype'] !='admin'){ // if session variable "username" does not exist.
header("location:index.php?msg=Please%20login%20to%20access%20admin%20area%20!"); // Re-direct to index.php
}else{
	include_once "db.php"; 
	error_reporting (E_ALL ^ E_NOTICE);
  //include_once "header.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Welcome to Stock Management System !</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<link rel="stylesheet" href="css/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<script src="js/jquery.min.js" type="text/javascript"></script>
		<script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="js/jquery.validationEngine.js" type="text/javascript"></script>
		<script src="js/jquery.hotkeys-0.7.9.js"></script>
		<script src="js/common.js"> </script>
		<link rel="stylesheet" href="rok_style.css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><table width="960" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
          <tr>
            <td height="90" align="left" valign="top"><img src="images/topbanner.jpg" width="960" height="82"></td>
          </tr>
          <tr>
            <td height="800" align="left" valign="top"><table width="960" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
              <tr>
                <td width="130" align="left" valign="top">
				
				<br>

				<strong>Welcome <font color="#3399FF"><?php echo $_SESSION['username']; ?> !</font></strong><br> <br>
				<?php include 'sidemenu.php';?>				
				</td> <td height="500" align="center" valign="top">
				<?php include_once 'menu.php';?>
			<?php
				if(isset($_POST['id']) && isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['address']) && !empty($_POST['address']) &&
				isset($_POST['contact1']) && !empty($_POST['contact1'])){	
			
					$id=mysql_real_escape_string($_POST['id']);
					$name=mysql_real_escape_string($_POST['name']);
					$address=mysql_real_escape_string($_POST['address']);
					$contact1=mysql_real_escape_string($_POST['contact1']);
					$contact2=mysql_real_escape_string($_POST['contact2']);
				
					if($db->query("UPDATE customer_details  SET customer_name='$name',customer_address='$address',customer_contact1='$contact1',customer_contact2='$contact2' where id=$id"))
						echo "<br><font color=green size=+1 > [ $name ] Supplier Details Updated!</font>" ;
					else
						echo "<br><font color=red size=+1 >Problem in Updation !</font>" ;
				}
				
				?>
				
				<br><br>

				<?php 
					if(isset($_GET['sid']))
						$id=$_GET['sid'];
						$line = $db->queryUniqueObject("SELECT * FROM customer_details WHERE id=$id");
				?>
				<form name="form1" method="post" id="form1" action="">
                   <input name="id" type="hidden" value="<?php echo $_GET['sid']; ?>">  
                  <p align="center"><strong>Update  Suplier Details</strong></p>
                  <table width="300"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">Name:</td>
                      <td width="150"><input name="name" type="text" id="name"  class="validate[required,length[0,100]] text-input" value="<?php echo $line->customer_name; ?>"></td>
                    </tr>
                    <tr>
                      <td width="150">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150">Address</td>
                      <td width="150"><textarea name="address" cols="15"><?php echo $line->customer_address;?></textarea></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Contact 1 </td>
                      <td><input name="contact1" type="text" id="buyingrate"  class="validate[optional,custom[onlyNumber],length[6,15]] text-input" value="<?php echo $line->customer_contact1;?>"></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Contact 2 </td>
                      <td><input name="contact2" type="text" id="sellingrate"  class="validate[optional,custom[onlyNumber],length[6,15]] text-input" value="<?php echo $line->customer_contact2;?>" ></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="right"><input type="reset" name="Reset" value="Reset">                     &nbsp;&nbsp;&nbsp;</td>
                      <td>                        &nbsp;&nbsp;&nbsp;
                        <input type="submit" name="Submit" value="Save"></td>
                    </tr>
                    <tr>
                      <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Control + R) </td>
                      <td align="left"> &nbsp;&nbsp;( Control + S ) </td>
                    </tr>
                  </table>
                </form>
				<br>
<br>

				</td>
              </tr>
            </table>
			
		</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#72C9F4"><span class="style1"><a href="http://www.pluskb.com">Developed by PlusKB Innovations</a></span></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

</body>
</html>
<?php
}
?>